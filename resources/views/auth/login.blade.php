@extends('login/mastertheme')
@section('title','Login')

@section('css')
@endsection


@section('content')
<form action="{{ route('login') }}" method="post">
    @csrf
    @if(Session('errors'))
    <div class="alert alert-dismissible bg-danger text-white border-0 fade show" role="alert">
        Something it's wrong:
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if(Session::has('success'))
    <div class="alert alert-dismissible bg-success text-white border-0 fade show">
        {{ Session::get('success') }}
    </div>
    @endif

    @if (Session::has('error'))
    <div class="alert alert-dismissible bg-danger text-white border-0 fade show">
        {{ Session::get('error') }}
    </div>
    @endif

    <div class="form-group">
        <label class="text-label" for="email_2">Email Address:</label>
        <div class="input-group input-group-merge">
            <input id="email" name="email" type="email" required="" class="form-control form-control-prepended">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="far fa-envelope"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="password_2">Password:</label>
        <div class="input-group input-group-merge">
            <input id="password" name="password" type="password" required="" class="form-control form-control-prepended" placeholder="Enter your password">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-key"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group mb-5">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" checked="" id="remember">
            <label class="custom-control-label" for="remember">Remember me</label>
        </div>
    </div>
    <div class="form-group text-center">
        <button class="btn btn-primary mb-5" type="submit">Login</button><br>
        <!--            <a href="">Forgot password?</a> <br>-->
        <!--            Don't have an account? <a class="text-body text-underline" href="signup.html">Sign up!</a>-->
    </div>
</form>
@endsection

@section('js')
@endsection
