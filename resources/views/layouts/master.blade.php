<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield("title","PT Agung Trisula Mandiri")</title>
    <meta name="robots" content="noindex">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon.png') }}">

    <!-- Simplebar -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/vendor/simplebar.min.css" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/app.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/app.rtl.css" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-material-icons.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-material-icons.rtl.css" rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-fontawesome-free.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-fontawesome-free.rtl.css" rel="stylesheet">

    <link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <!-- Select 2 -->
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-select2.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/css/vendor-select2.rtl.css" rel="stylesheet">
    <link type="text/css" href="{{ asset('assets/theme/dist') }}/assets/vendor/select2/select2.min.css" rel="stylesheet">
    
    @yield('css')
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=327167911228268&ev=PageView&noscript=1" /></noscript>

    
    <style>
        .wajib{color: red;}
        .wajib:after{content: "*"}
    </style>
</head>

<body class="layout-default">
    <div class="preloader"></div>
    
    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">
    
    <!-- Header -->
    @include('layouts.navbar')
    <!-- // END Header -->
    
    <!-- Header Layout Content -->
    <div class="mdk-header-layout__content">
        <div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="992px">
            <div class="mdk-drawer-layout__content page">
                @yield('isi')
                @yield('content')
            </div>
            <!-- // END drawer-layout__content -->
            @include('layouts.sidebar')

            <!-- menu samping -->
        </div>
    </div>
    <!-- // END header-layout -->
</div>

<!-- App Settings FAB -->

<!-- jQuery -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/jquery.min.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/jquery.mask.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/jquery.maskMoney.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/jquery-ui.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>

<!-- Select2 -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/select2/select2.min.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/select2.js"></script>

<!-- Bootstrap -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/popper.min.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/bootstrap.min.js"></script>

<!-- Simplebar -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/simplebar.min.js"></script>

<!-- DOM Factory -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/dom-factory.js"></script>

<!-- MDK -->
<script src="{{ asset('assets/theme/dist') }}/assets/vendor/material-design-kit.js"></script>

<!-- App -->
<script src="{{ asset('assets/theme/dist') }}/assets/js/toggle-check-all.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/check-selected-row.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/dropdown.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/sidebar-mini.js"></script>
<script src="{{ asset('assets/theme/dist') }}/assets/js/app.js" defer></script>

<!-- App Settings (safe to remove) -->
<script src="{{ asset('assets/theme/dist') }}/assets/js/app-settings.js"></script>

@yield('js')
</body>

</html>
