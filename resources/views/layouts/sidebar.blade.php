<div class="mdk-drawer  js-mdk-drawer" id="default-drawer" data-align="start">
    <div
        class="mdk-drawer__content">
        <div class="sidebar sidebar-light sidebar-left simplebar" data-simplebar>
            @if(!Auth::user())
            <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
                <a href="{{ route('login') }}" class="flex d-flex align-items-center text-underline-0 text-body">
                                    <span class="avatar mr-3">
                                        <img src="{{ asset('assets/theme/dist') }}/assets/images/atm.jpg" alt="avatar" class="avatar-img rounded-circle">
                                    </span>
                    <span class="flex d-flex flex-column"><strong>LOGIN</strong></span>
                </a>
            </div>
            @else
            <div class="d-flex align-items-center sidebar-p-a border-bottom sidebar-account">
                <a href="{{ route('home') }}" class="flex d-flex align-items-center text-underline-0 text-body">
                                    <span class="avatar mr-3">
                                        <img src="{{ asset('assets/theme/dist') }}/assets/images/avatar/demi.png" alt="avatar" class="avatar-img rounded-circle">
                                    </span>
                    <span class="flex d-flex flex-column">
                            <strong>{{ Auth::user()->name }}</strong>
                            <!--<small class="text-muted text-uppercase">Account Manager</small>-->
                    </span>
                </a>

                <div class="dropdown ml-auto">
                    <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"><i class="material-icons">more_vert</i></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-item-text dropdown-item-text--lh">
                            <div><strong>{{ Auth::user()->name }}</strong></div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('home') }}">Dashboard</a>
                        <!--                        <a class="dropdown-item" href="profile.html">My profile</a>-->
                        <!--                        <a class="dropdown-item" href="edit-account.html">Edit account</a>-->
                        <!--                        <div class="dropdown-divider"></div>-->
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
            @endif

            <div class="sidebar-heading sidebar-m-t">Menu</div>
                <ul class="sidebar-menu">
                    <li class="sidebar-menu-item {{ request()->is('companies/*') ? 'active open' : '' }}">
                        <a class="sidebar-menu-button" href="{{ route('companies.index') }}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">business</i>
                            <span class="sidebar-menu-text">Companies</span>
                        </a>
                    </li>

                    <li class="sidebar-menu-item {{ request()->is('employees/*') ? 'active open' : '' }}">
                        <a class="sidebar-menu-button" href="{{ route('employees.index') }}">
                            <i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">account_circle</i>
                            <span class="sidebar-menu-text">Employees</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
