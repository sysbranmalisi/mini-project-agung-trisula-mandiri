@extends('layouts/master')
@section('title','Dashboard')

@section('css')
@endsection


@section('content')
<div class="mdk-drawer-layout__content page">
    <div class="container-fluid page__heading-container">
        <div class="page__heading d-flex align-items-center">
            <div class="flex">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
                <h1 class="m-0">Dashboard</h1>
            </div>
            <!-- <a href="" class="btn btn-light ml-3"><i class="material-icons icon-16pt text-muted mr-1">settings</i> Settings</a> -->
        </div>
    </div>

    <?php
        $companies                 = DB::table('companies')->count();
        $employees                 = DB::table('employees')->count();

        ?>


    <div class="container-fluid page__container">

        <div class="row card-group-row">
            
            <div class="col-lg-4 col-md-6 card-group-row__col">
                <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center">
                    <div class="flex">
                        <div class="card-header__title text-muted mb-2">Companies</div>
                        <div class="text-amount">{{ $companies }}</div>
                        <!-- <div class="text-stats text-success"></div> -->
                    </div>
                    <div><i class="material-icons icon-muted icon-40pt ml-3">business</i></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 card-group-row__col">
                <div class="card card-group-row__card card-body card-body-x-lg flex-row align-items-center">
                    <div class="flex">
                        <div class="card-header__title text-muted mb-2">Empoyees</div>
                        <div class="text-amount">{{ $employees }}</div>
                        <!-- <div class="text-stats text-success">456</div> -->
                    </div>
                    <div><i class="material-icons icon-muted icon-40pt ml-3">account_circle</i></div>
                </div>
            </div>
        </div>
    </div>


</div>
<!-- // END drawer-layout__content -->
@endsection

@section('js')
@endsection
