@extends('layouts/master')
@section('title','Create New Companies')
@section('content')

<div class="container-fluid  page__heading-container">
    <div class="page__heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
            </ol>
        </nav>
        <h1 class="m-0">@yield('title')</h1>
    </div>
</div>

<div class="container-fluid page__container">

    <div class="card card-form">
        <div class="row no-gutters">
            <div class="col-lg-4 card-body">
                <p><strong class="headings-color">Forms @yield('title') </strong></p>
                <p class="text-muted">Please fillout companies data<span class="font-weight-bold"></span></p>
            </div>
            <div class="col-lg-8 card-form__body card-body">
                <form method="POST" role="form" action="{{ route('companies.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6 mb-3">
                            <div class="form-group">
                                <label for="name" class="col-form-label">Name: <span class="wajib"></span></label>
                                <input name="name" class="form-control" placeholder="Name" type="text" required>
                            </div>

                            <div class="form-group">
                                <label for="logo" class="col-form-label">Logo: <span class="wajib"></span></label>
                                <input name="logo" class="form-control" placeholder="Logo" type="file" required>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 mb-3">
                            <div class="form-group">
                                <label for="email" class="col-form-label" style="padding-top: 11px;">Email: <span class="wajib"></span></label>
                                <input name="email" class="form-control" placeholder="Email" type="text" required>
                            </div>

                            <div class="form-group">
                                <label for="website" class="col-form-label" style="padding-top: 11px;">Website: <span class="wajib"></span></label>
                                <input name="website" class="form-control" placeholder="Website" type="text" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection