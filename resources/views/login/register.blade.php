@extends('login/mastertheme')
@section('title','Register Account')

@section('css')
@endsection


@section('content')
<form action="{{ route('register') }}">
    @csrf


    <div class="form-group">
        <label class="text-label" for="email_2">Nama Lengkap</label>
        <div class="input-group input-group-merge">
            <input id="name" name="name" type="text" class="form-control form-control-prepended @error('name') is-invalid @enderror" placeholder="Nama Lengkap" required autocomplete="name" autofocus>
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="far fa-envelope"></span>
                </div>
            </div>
            @error('name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="email_2">Email Address</label>
        <div class="input-group input-group-merge">
            <input id="email" name="email" type="email" class="form-control form-control-prepended @error('email') is-invalid @enderror" placeholder="john@doe.com" required autocomplete="email">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="far fa-envelope"></span>
                </div>
            </div>
            @error('email')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="password_2">Password</label>
        <div class="input-group input-group-merge">
            <input id="password" name="password" type="password" class="form-control form-control-prepended @error('password') is-invalid @enderror" placeholder="Enter your password" required autocomplete="new-password">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-key"></span>
                </div>
            </div>
            @error('password')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <label class="text-label" for="password_2">Password Confirm</label>
        <div class="input-group input-group-merge">
            <input id="password-confirm" name="password_confirmation" type="password" required autocomplete="new-password" class="form-control form-control-prepended" placeholder="Confirm your password">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <span class="fa fa-key"></span>
                </div>
            </div>

        </div>
    </div>
    <div class="form-group mb-5">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" checked="" id="remember">
            <label class="custom-control-label" for="remember">Remember me</label>
        </div>
    </div>
    <div class="form-group text-center">
        <button class="btn btn-primary mb-5" type="submit">{{ __('Register') }}</button><br>
        <!--            <a href="">Forgot password?</a> <br>-->
        <!--            Don't have an account? <a class="text-body text-underline" href="signup.html">Sign up!</a>-->
    </div>
</form>
@endsection

@section('js')
@endsection
