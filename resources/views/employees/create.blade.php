@extends('layouts/master')
@section('title','Create New Employees')
@section('content')

<div class="container-fluid  page__heading-container">
    <div class="page__heading">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
            </ol>
        </nav>
        <h1 class="m-0">@yield('title')</h1>
    </div>
</div>

<div class="container-fluid page__container">

    <div class="card card-form">
        <div class="row no-gutters">
            <div class="col-lg-4 card-body">
                <p><strong class="headings-color">Forms @yield('title') </strong></p>
                <p class="text-muted">Please fillout employees data<span class="font-weight-bold"></span></p>
            </div>
            <div class="col-lg-8 card-form__body card-body">
                <form method="POST" role="form" action="{{ route('employees.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6 mb-3">
                            <div class="form-group">
                                <label for="first_name" class="col-form-label">First Name: <span class="wajib"></span></label>
                                <input name="first_name" class="form-control" placeholder="First Name" type="text" required>
                            </div>

                            <label class="col-form-label">Companies <span class="wajib"></span></label>
                            <select name="id_companies" id="id_companies" class="form-control" required>
                                <option value="">== Select Companies ==</option>
                                @foreach ($companies as $data)
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                            </select>

                            <div class="form-group">
                                <label for="phone" class="col-form-label" style="padding-top: 11px;">Phone: <span class="wajib"></span></label>
                                <input name="phone" class="form-control" placeholder="Phone" type="number" required>
                            </div>
                        </div>

                        <div class="col-12 col-md-6 mb-3">
                            <div class="form-group">
                                <label for="last_name" class="col-form-label">Last Name: <span class="wajib"></span></label>
                                <input name="last_name" class="form-control" placeholder="Last Name" type="text" required>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-form-label" style="padding-top: 11px;">Email: <span class="wajib"></span></label>
                                <input name="email" class="form-control" placeholder="Email" type="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button class="btn btn-success" type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

@endsection