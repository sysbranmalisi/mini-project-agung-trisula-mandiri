@extends('layouts/master')
@section('title','Employees')

@section('content')

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="#"><i class="material-icons icon-20pt">home</i></a></li>
                    <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
                </ol>
            </nav>
            <h1 class="m-0">@yield('title')</h1>
        </div>
        <a href="{{ route('employees.create') }}" class="btn btn-success ml-3">Create <i class="material-icons">add</i></a>
    </div>
</div>

<div class="container-fluid page__container">
    <div class="card">
        <div class="card-header card-header-large">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="card-header__title">@yield('title')</h4>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row align-items-end">
                <div class="card-body"> 
                    <div class="row">
                        <div class="table-responsive m-t-40">
                            <table id="employees_table" class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th style="width: 5%;">No. </th>  
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Companies</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>                            
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endsection

@section('js')

<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

<script>
$('#employees_table').DataTable({
    responsive: true,
    processing: false,
    serverSide: true,
    ajax: "{{route("employees.getDataEmployees")}}",
    columns: [
        {
            data: null, sortable: false, render: function (data, type, row, meta) {
                var i = meta.row + meta.settings._iDisplayStart + 1;
                return i
            }
        },
        { data: 'first_name', name: 'first_name'},
        { data: 'last_name', name: 'last_name'},
        { data: 'id_companies', name: 'id_companies'},
        { data: 'email', name: 'email'},
        { data: 'phone', name: 'phone'},
    ]
});

</script>
@endsection