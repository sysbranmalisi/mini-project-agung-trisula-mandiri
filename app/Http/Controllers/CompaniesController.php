<?php

namespace App\Http\Controllers;

use App\Models\Companies;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Mail;
use Intervention\Image\ImageManagerStatic as Image;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('companies/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDataCompanies()
    {
        $companies  = Companies::all();

        return Datatables::of($companies)
        ->addColumn('logo', function ($companies) {
            $url= asset('../storage/app/public/companies_logos/'.$companies->logo);
            return '<img src="'.$url.'" border="0" width="100" align="center" />';
        })
        ->rawColumns(['image', 'logo'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name',
            'email',
            'logo',
            'website',
        ]);

        if ($request->file('logo')){
            $image              = $request->file('logo');
            $input['imagename'] = time().'.'.$image->extension();

            $destinationPath    = public_path('../storage/app/public/companies_logos');
            $img                = Image::make($image->path());
            $img->resize(1000, 1000, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'.$input['imagename']);

            $image->move($destinationPath, $input['imagename']);
        }

        Companies::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'logo'      => $input['imagename'],
            'website'   => $request->website,
        ]);

        $subject    = "New Companies of Agung Trisula Mandiri";
        $message    = "This is A New Companies";
        $email      = $request->email;

        $data = array(
            'from' => 'smtp.mailtrap.io',
            'to' => $request->email,
            'subject' => 'New Companies',
            'messagenote' => 'This is a new companies',
        );

        Mail::to($request->email)->send(new \App\Mail\WelcomeMail());

        return redirect()->route('companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
