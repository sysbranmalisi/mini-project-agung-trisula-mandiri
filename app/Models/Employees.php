<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table = 'employees';

    protected $fillable = [
        'first_name',
        'last_name',
        'id_companies',
        'email',
        'phone',
    ];

    public function getDataCompanies(){
        return $this->belongsTo(Companies::class,'id_companies','id');
    }
}
