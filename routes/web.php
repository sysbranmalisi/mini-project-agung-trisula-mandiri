<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    // return view('welcome');
    return view('/layouts/beranda');
})->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

// Companies
Route::get('companies/index', 'CompaniesController@index')->name('companies.index')->middleware('auth');
Route::get('companies/create', 'CompaniesController@create')->name('companies.create');
Route::get('companies/getDataCompanies', 'CompaniesController@getDataCompanies')->name('companies.getDataCompanies');
Route::post('companies/store', 'CompaniesController@store')->name('companies.store');

Route::get('/companies/email', function () {
    Mail::to('admin@admin.com')->send(new \App\Mail\WelcomeMail());
    return new \App\Mail\WelcomeMail();
});

// Employees
Route::get('employees/index', 'EmployeesController@index')->name('employees.index')->middleware('auth');
Route::get('employees/create', 'EmployeesController@create')->name('employees.create')->middleware('auth');
Route::get('employees/getDataEmployees', 'EmployeesController@getDataEmployees')->name('employees.getDataEmployees');
Route::post('employees/store', 'EmployeesController@store')->name('employees.store');
Route::post('employees/email/{id}', 'EmployeesController@email')->name('employees.email');